$(document).ready(function(){
  
	$('.text-field-input').on('focus', function(){
	  $(this).closest('.field-wrapper').addClass('focused');
	});
	
	$('.text-field-input').on('blur', function(){
   
	  if  ( $(this).val() === '') {
		 $(this).closest('.field-wrapper').removeClass('focused');
	  }
	});	

	$('#scroll-down').on('click', function(){
		$('html, body').animate({scrollTop: '+=800px'}, 800);
	})
})